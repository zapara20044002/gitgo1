from PyQt5.QtWidgets import QApplication, QMainWindow

import ip_form
import functions
import ip_form


class IpPreg(QMainWindow, ip_form.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.pushButton.clicked.connect(self.btn_click)

    def btn_click(self):
        octet1 = functions.dec_to_bin(int(self.lineEdit.text()))
        octet2 = functions.dec_to_bin(int(self.lineEdit_2.text()))
        octet3 = functions.dec_to_bin(int(self.lineEdit_3.text()))
        octet4 = functions.dec_to_bin(int(self.lineEdit_4.text()))

        if not 11111111 <= self.lineEdit.text() <= 0:

            while len(octet1) < 8:
            octet1 = "0" + f'{octet1}'
        while len(octet2) < 8:
            octet2 = "0" + f'{octet2}'
        while len(octet3) < 8:
            octet3 = "0" + f'{octet3}'
        while len(octet4) < 8:
            octet4 = "0" + f'{octet4}'

        self.lineEdit_5.setText(f'{octet1} | {octet2} | {octet3} | {octet4}')

if __name__ == '__main__':
    app = QApplication([])
    wnd = IpPreg()
    wnd.show()
    app.exec()
